from PIL import Image
from statistics import median
import math

def distance(r1,g1,b1,r2,g2,b2):
    dr = (r2 - r1) ** 2
    dg = (g2 - g1) ** 2
    db = (b2 - b1) ** 2
    d = dr + dg + db
    return d

im = Image.open('Chap8_Sauzon.jpg')
largeur = im.size[0]
hauteur = im.size[1]

im2 = Image.new('RGB', (largeur,hauteur))

for lig in range(hauteur):
    for col in range(largeur):

        (r,g,b) = im.getpixel((col,lig))
        
        # contiendra les distances de couleur entre le pixel courant et ceux qui l'entourent
        ds = []

        # à gauche
        # limite : bord gauche de l'image
        if (col - 1 > 0):
            (ra,ga,ba) = im.getpixel((col - 1, lig))
            ds.append(distance(r,g,b,ra,ga,ba))
            
            # en haut à gauche
            # limite : bord supérieur de l'image (y = 0)
            if (lig - 1 > 0):
                (ra,ga,ba) = im.getpixel((col - 1, lig - 1))
                ds.append(distance(r,g,b,ra,ga,ba))
                
            # en bas à gauche
            # limite : bord inférieur de l'image (y = hauteur)
            # hateur - 1 car on compte à partir de 0
            if (lig + 1 < hauteur - 1):
                (ra,ga,ba) = im.getpixel((col - 1, lig + 1))
                ds.append(distance(r,g,b,ra,ga,ba))
                
        # à droite
        # limite : bord droit de l'image (x = largeur)
        # largeur - 1 car on compte à partir de 0
        if (col + 1 < largeur - 1):
            (ra,ga,ba) = im.getpixel((col + 1, lig))
            ds.append(distance(r,g,b,ra,ga,ba))
            
            # en haut à droite
            # limite : bord supérieur de l'image (y = 0)
            if (lig - 1 < 0):
                (ra,ga,ba) = im.getpixel((col + 1, lig - 1))
                ds.append(distance(r,g,b,ra,ga,ba))
                
            # en bas à droite
            # limite : bord inférieur de l'image (y = hauteur)
            # hateur - 1 car on compte à partir de 0
            if (lig + 1 < hauteur - 1):
                (ra,ga,ba) = im.getpixel((col + 1, lig + 1))
                ds.append(distance(r,g,b,ra,ga,ba))
                
        # en haut
        # limite : bord supérieur de l'image (y = 0)
        if (lig - 1 < 0):
            (ra,ga,ba) = im.getpixel((col, lig - 1))
            ds.append(distance(r,g,b,ra,ga,ba))
            
        # en bas
        # limite : bord inférieur de l'image (y = hauteur)
        # hateur - 1 car on compte à partir de 0
        if (lig + 1 < hauteur - 1):
            (ra,ga,ba) = im.getpixel((col, lig + 1))
            ds.append(distance(r,g,b,ra,ga,ba))

        # moyenne des distances
        dm = sum(ds) / len(ds)

        if (dm > 1000):
            im2.putpixel((col,lig), (0,0,0))
        else:
            im2.putpixel((col,lig), (255,255,255))
im2.save('5_contour.jpg')
