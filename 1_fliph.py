from PIL import Image


im = Image.open('linux.jpg')
largeur = im.size[0]
hauteur = im.size[1]

im2 = Image.new('RGB', (largeur,hauteur))

for lig in range(hauteur):
    for col in range(largeur):

        (r,g,b) = im.getpixel((col,lig))

        ncol = (largeur - col) - 1

        im2.putpixel((ncol,lig), (r,g,b))
im2.save('1_fliph.jpg')
