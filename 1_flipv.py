from PIL import Image


im = Image.open('linux.jpg')
largeur = im.size[0]
hauteur = im.size[1]

im2 = Image.new('RGB', (largeur,hauteur))

for lig in range(hauteur):
    for col in range(largeur):

        (r,g,b) = im.getpixel((col,lig))

        nlig = (hauteur - lig) - 1

        im2.putpixel((col,nlig), (r,g,b))
im2.save('1_flipv.jpg')
