from PIL import Image
import math


im = Image.open('Chap8_Sauzon.jpg')
largeur = im.size[0]
hauteur = im.size[1]

im2 = Image.new('RGB', (largeur,hauteur))

for lig in range(hauteur):
    for col in range(largeur):

        (r,g,b) = im.getpixel((col,lig))

        # math.floor convertit en nombre entier
        r1 = math.floor(r / 2)
        g1 = math.floor(g / 2)
        b1 = math.floor(b / 2)
        
        im2.putpixel((col,lig), (r1,g1,b1))
im2.save('3_assombrir.jpg')
