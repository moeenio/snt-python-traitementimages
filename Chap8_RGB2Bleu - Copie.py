from PIL import Image
im = Image.open('Chap8_chat.jpg')
largeur = im.size[0]
hauteur = im.size[1]
im2 = Image.new('RGB', (largeur,hauteur))
print(largeur,hauteur)
for lig in range(hauteur):
    for col in range(largeur):
        (r,g,b) = im.getpixel((col,lig))
        im2.putpixel((col,lig), (0,0,b))
im2.save('Chap8_chatbleu.jpg')
